# fmcclient

A Go client for the Cisco Firepower Management Center.

## Example

```go
import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"time"

	"idiolink/go/fmcclient"
)

host := "192.0.2.10"
user := "fmcrobot"
pass := "anpassword"
proxy := "socks5://127.0.0.1:8000"

/* Configure socks proxy and other transport parameters. */
tr := &http.Transport{
	TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
	TLSHandshakeTimeout:   15 * time.Second,
	IdleConnTimeout:       30 * time.Minute,
	ResponseHeaderTimeout: 60 * time.Second,
	ExpectContinueTimeout: 10 * time.Second,
}
var proxyURL *url.URL
proxyURL, err = url.Parse(proxy)
if err != nil {
	log.Fatal(err)
}
if proxyURL != nil {
	tr.Proxy = http.ProxyURL(proxyURL)
}
cl := &http.Client{
	Transport: tr,
	Timeout:   30 * time.Minute,
}

/* Initiate FMC client state and authenticate to FMC. Note
that authentication tokens will be automatically refreshed,
and new access tokens will be automatically requested after
the maximum number of token refreshes (3) has been reached.
Nevertheless, the `Authenticate()` method may be called as
often as desired: authenticating an authenticated FMC
client instance is a no-op. */
ctx, cancel := context.WithCancel(context.Background())
var fmc *fmcclient.FMC
fmc, err := fmcclient.New(ctx, cl, host, user, pass)
if err != nil {
	log.Fatal(err)
}
if err := fmc.Authenticate(); err != nil {
	log.Fatal(err)
}

/* Create pager for ACPs and iterate to find matching name.
The maximum page size, in accordance with Cisco
documentation, is 1000. */
var pager fmcclient.FMCAccessPoliciesPager
pager, err := fmc.AccessPolicies("Global", 100)
if err != nil {
	log.Fatal(fmt.Sprintf("fetch access policy pager: %v", err))
}
acpName := "test"
var acp *fmcclient.FMCAccessPolicy
for _, e := pager.Iter()
	if pager.LastErr() != nil {
		log.Fatal(fmt.Sprintf("fetch access policies: %v", err))
	}
	if e.Name == acpName {
		acp = e.Id
		break
	}
}

if acp.Id == "" {
	fmt.Printf("find target access policy: no acp with name %v\n", acpName)
	return
}
fmt.Printf("Found target access policy %v\n", acp.Id)
```
